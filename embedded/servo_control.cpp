#include <thread>
#include <chrono>
#include <cstdio>
#include <cppgpio.hpp>

std::chrono::milliseconds period = std::chrono::milliseconds(20);
const auto p0 = std::chrono::system_clock::now();

void moveServoTo(int degree, GPIO::DigitalOut &servo, std::chrono::microseconds &imp) {
        imp = std::chrono::microseconds(500+degree*11);
        printf("%i", &imp);
        servo.on();
        std::this_thread::sleep_for(imp);
        printf("juz po");
        servo.off();
    }

int main() {
    GPIO::DigitalOut s1(3);
    std::chrono::microseconds imp_s1;

    GPIO::DigitalOut s2(5);
    std::chrono::microseconds imp_s2;
    while(true){
        std::thread servo1(moveServoTo, 20, std::ref(s1), std::ref(imp_s1));
        std::thread servo2(moveServoTo, 100, std::ref(s2), std::ref(imp_s2));
        servo1.join();
        servo2.join();
        std::this_thread::sleep_for(period - (imp_s1 > imp_s2 ? imp_s1 : imp_s2));
    }
}