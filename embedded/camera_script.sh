#!/bin/bash
rpicam-vid --width 1920 --height 1080 --framerate 30 --shutter 16666 --gain 4 -n -t 0 -o - | ffmpeg -err_detect ignore_err -probesize 10M -use_wallclock_as_timestamps 1 -i - -pix_fmt yuv420p -b:v 25M -c copy -f rtsp rtsp://localhost:8554/cam
