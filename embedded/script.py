import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)
GPIO.setup(3,GPIO.OUT)
GPIO.setup(5,GPIO.OUT)
cycle = 0.02
left = 0.0015
right = 0.0015
step1 = left
step2 = right
inc = 0
while(True):
    if step1 < right and inc > 0:
        inc*=-1
    if step1 > left and inc < 0:
        inc*=-1
    GPIO.output(3,True)
    time.sleep(step1)
    GPIO.output(3,False)
    time.sleep(cycle-step1)
    GPIO.output(5,True)
    time.sleep(step2)
    GPIO.output(5,False)
    time.sleep(cycle-step2)
    print('step1: ', step1)
    print('step2: ', step2)
    print('inc: ', inc)
    step1-=inc
    step2+=inc

